#!/bin/sh

set -e
cd debian/tests/test-tunneled-ssh.d

#Configure apache2 and /etc/hosts
./configure-sites

#Create a dedicated user to run the actual tests
TESTUSER=$(mktemp -u _uXXXXXXXX)
echo Test user is $TESTUSER.
useradd -m $TESTUSER

#Configure ssh as user $TESTUSER
su -l $TESTUSER $PWD/configure-ssh $PWD/ssh_config

#Run tests as user $TESTUSER
# Test against the HTTP proxy
su -l $TESTUSER $PWD/run-test vhost80 80

# Test against the HTTPS proxy, partly restricted to certain TLS versions
for protocol in TLSv1.2 TLSv1.3 all; do
	./set-sslprotocol "$protocol"
	su -l $TESTUSER $PWD/run-test vhost443 443
done

# Run as daemon listing on an IPv4 address and test against the HTTPS proxy 
su -l $TESTUSER -c "proxytunnel -a 127.0.1.1:2222 -Ezp vhost443:443 -P vhost443-user:vhost443-password -d localhost:22 &
$PWD/run-test vhost443 443 127.0.1.1 2222 &&
pkill proxytunnel"

# Run as daemon listing on an IPv6 address and test against the HTTPS proxy 
su -l $TESTUSER -c "proxytunnel -a [::1]:2222 -Ezp vhost443:443 -P vhost443-user:vhost443-password -d localhost:22 &
$PWD/run-test vhost443 443 ::1 2222 &&
pkill proxytunnel"
